

# Requerimientos


- [Java 8 runtime environment (SE JRE)](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven 3](https://maven.apache.org/docs/history.html)


# Compilación


Sobre el directorio del proyecto ejecutar el siguiente comando maven

    $ mvn install


# Configuración de la función Lambda


Para configurar la función lambda se deben definir las siguientes variables de entorno:

Clave "BD" - Valor "nombre de la base"

Clave "ENDPOINT" - Valor "https://rds-data.us-east-1.amazonaws.com"

Clave "QUERY_CONVERSION_FCI" - Valor "select FactorConversion from cotizacion_fci order by fecha desc limit 1;"

Clave "QUERY_TENENCIA_CONSOLIDADA" - Valor "select sum(TenenciaVal) from tenenciaval where productor_id=%s and TpActivo NOT IN ( 'Pesos', 'Dolares' )"

Clave "QUERY_TENENCIA_LIQ_DOLAR" - Valor "select sum(Cantidad) from tenenciaval where productor_id=%s and tpActivo='Dolares'"

Clave "QUERY_TENENCIA_LIQ_PESO" - Valor "select sum(TenenciaVal) from tenenciaval where productor_id=%s and tpActivo='Pesos'"

Clave "REGION" - Valor "us-east-1"

Clave "RESOURCE_ARN" - Valor "resource arn de aurora"

Clave "SECRET_ARN" - Valor "secret arn de aurora"


# Despliegue


Cargar en la función lambda el .jar generado en el directorio ..\partner-productor-tenencia\target


# Test

Ejemplo JSON de entrada:

    {
      "idProductor": 96
    }
    
Ejemplos JSON respuesta:

    {
     "tenencias": [
    {
      "tpTenencia": "Liquida",
      "valorPesos": "0.00",
      "valorDolares": "24899.34"
    },
    {
      "tpTenencia": "Consolidada",
      "valorPesos": "9237907.92",
      "valorDolares": "82400.39"
    }
    ],
	  "tenenciaTotalPesos": "9237907.92",
	  "tenenciaTotalDolares": "107299.73",
	  "valorDolarFci": "112.11"
	    }
	  
