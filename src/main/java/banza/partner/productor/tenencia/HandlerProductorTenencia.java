package banza.partner.productor.tenencia;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.rdsdata.AWSRDSData;
import com.amazonaws.services.rdsdata.AWSRDSDataClientBuilder;
import com.amazonaws.services.rdsdata.model.ExecuteStatementRequest;
import com.amazonaws.services.rdsdata.model.ExecuteStatementResult;
import com.amazonaws.services.rdsdata.model.Field;

import banza.partner.productor.tenencia.dto.Params;
import banza.partner.productor.tenencia.dto.TenenciaDto;
import banza.partner.productor.tenencia.dto.TenenciaResponseDto;

public class HandlerProductorTenencia implements RequestHandler<Params, TenenciaResponseDto> {

	private static String TP_LIQUIDA = "Liquida";
	private static String TP_CONSOLIDADA = "Consolidada";

	@Override
	public TenenciaResponseDto handleRequest(Params event, Context context) {
		AWSRDSData rdsData = AWSRDSDataClientBuilder.standard()
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(System.getenv("ENDPOINT"), System.getenv("REGION")))
				.build();

		// Obtengo el valor de la tenencia liquida en pesos
		ExecuteStatementRequest request = new ExecuteStatementRequest().withResourceArn(System.getenv("RESOURCE_ARN"))
				.withSecretArn(System.getenv("SECRET_ARN")).withDatabase(System.getenv("BD"))
				.withSql(String.format(System.getenv("QUERY_TENENCIA_LIQ_PESO"), event.getIdProductor()));
		ExecuteStatementResult result = rdsData.executeStatement(request);
		List<TenenciaDto> listTenencias = new ArrayList<TenenciaDto>();
		String tenenciaLiquidaPesos = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				if (fields.get(0).getDoubleValue() != null && fields.get(0).getDoubleValue() > 0) {
					tenenciaLiquidaPesos = new DecimalFormat("0.00").format((fields.get(0).getDoubleValue()));
				}
			}
		}

		// Obtengo el valor de la tenencia liquida en dolares
		request.withSql(String.format(System.getenv("QUERY_TENENCIA_LIQ_DOLAR"), event.getIdProductor()));
		result = rdsData.executeStatement(request);
		String tenenciaLiquidaDolares = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				if (fields.get(0).getDoubleValue() != null && fields.get(0).getDoubleValue() > 0) {
					tenenciaLiquidaDolares = new DecimalFormat("0.00").format((fields.get(0).getDoubleValue()));
				}
			}
		}

		// Seteo la tenencia liquida
		TenenciaDto tenenciaLiquida = new TenenciaDto();
		tenenciaLiquida.setTpTenencia(TP_LIQUIDA);
		tenenciaLiquida.setValorPesos(tenenciaLiquidaPesos);
		tenenciaLiquida.setValorDolares(tenenciaLiquidaDolares);
		listTenencias.add(tenenciaLiquida);

		// Obtengo la tenencia consolidada en pesos
		request.withSql(String.format(System.getenv("QUERY_TENENCIA_CONSOLIDADA"), event.getIdProductor()));
		result = rdsData.executeStatement(request);
		String tenenciaConsolidadaPesos = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				if (fields.get(0).getDoubleValue() != null && fields.get(0).getDoubleValue() > 0) {
					tenenciaConsolidadaPesos = new DecimalFormat("0.00").format((fields.get(0).getDoubleValue()));
				}

			}
		}

		// Obtengo el valor del dolar fci para calcular la tenencia consolidada en
		// dolares
		request.withSql(System.getenv("QUERY_CONVERSION_FCI"));
		result = rdsData.executeStatement(request);
		BigDecimal valorDolarFCI = new BigDecimal(0);
		String tenenciaConsolidadaDolares = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				valorDolarFCI = new BigDecimal(fields.get(0).getStringValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
				tenenciaConsolidadaDolares = new BigDecimal(tenenciaConsolidadaPesos)
						.divide(valorDolarFCI, 2, RoundingMode.HALF_UP).toString();
			}
		}

		// Seteo la tenencia consolidada
		TenenciaDto tenenciaConsolidada = new TenenciaDto();
		tenenciaConsolidada.setTpTenencia(TP_CONSOLIDADA);
		tenenciaConsolidada.setValorPesos(tenenciaConsolidadaPesos);
		tenenciaConsolidada.setValorDolares(tenenciaConsolidadaDolares);
		listTenencias.add(tenenciaConsolidada);

		// Lleno el response
		TenenciaResponseDto tenenciasResponse = new TenenciaResponseDto();
		tenenciasResponse.setTenencias(listTenencias);
		tenenciasResponse.setValorDolarFci(valorDolarFCI.toString());
		tenenciasResponse.setTenenciaTotalPesos(
				new BigDecimal(tenenciaConsolidadaPesos).add((new BigDecimal(tenenciaLiquidaPesos))).toString());
		tenenciasResponse.setTenenciaTotalDolares(
				new BigDecimal(tenenciaConsolidadaDolares).add((new BigDecimal(tenenciaLiquidaDolares))).toString());
		return tenenciasResponse;
	}
}