package banza.partner.productor.tenencia;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class test {

	public static void main(String[] args) {
		 BigDecimal res; 
		 String input1 
         = "204800000.34"; 
     String input2 
         = "256.11"; 

     // Convert the string input to BigDecimal 
     BigDecimal a 
         = new BigDecimal(input1); 
     BigDecimal divisor 
         = new BigDecimal(input2); 

     // Using divide() method 
     res = a.divide(divisor,2, RoundingMode.HALF_UP); 

     // Display the result in BigDecimal 
     System.out.println(res); 
	}

}
