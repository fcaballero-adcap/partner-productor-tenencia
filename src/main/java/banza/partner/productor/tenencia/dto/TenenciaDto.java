package banza.partner.productor.tenencia.dto;

public class TenenciaDto {

	private String tpTenencia;

	private String valorPesos;

	private String valorDolares;

	public TenenciaDto() {
		super();
		this.valorPesos = "0.00";
		this.valorDolares = "0.00";
	}

	public String getTpTenencia() {
		return tpTenencia;
	}

	public String getValorPesos() {
		return valorPesos;
	}

	public String getValorDolares() {
		return valorDolares;
	}

	public void setTpTenencia(String tpTenencia) {
		this.tpTenencia = tpTenencia;
	}

	public void setValorPesos(String valorPesos) {
		this.valorPesos = valorPesos;
	}

	public void setValorDolares(String valorDolares) {
		this.valorDolares = valorDolares;
	}

}