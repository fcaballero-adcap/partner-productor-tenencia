package banza.partner.productor.tenencia.dto;

import java.util.List;

public class TenenciaResponseDto {

	private List<TenenciaDto> tenencias;

	private String tenenciaTotalPesos;

	private String tenenciaTotalDolares;

	private String valorDolarFci;

	public List<TenenciaDto> getTenencias() {
		return tenencias;
	}

	public void setTenencias(List<TenenciaDto> tenencias) {
		this.tenencias = tenencias;
	}

	public String getValorDolarFci() {
		return valorDolarFci;
	}

	public void setValorDolarFci(String valorDolarFci) {
		this.valorDolarFci = valorDolarFci;
	}

	public String getTenenciaTotalPesos() {
		return tenenciaTotalPesos;
	}

	public String getTenenciaTotalDolares() {
		return tenenciaTotalDolares;
	}

	public void setTenenciaTotalPesos(String tenenciaTotalPesos) {
		this.tenenciaTotalPesos = tenenciaTotalPesos;
	}

	public void setTenenciaTotalDolares(String tenenciaTotalDolares) {
		this.tenenciaTotalDolares = tenenciaTotalDolares;
	}


}